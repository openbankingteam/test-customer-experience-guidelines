# About these guidelines

## These guidelines cover the core use cases that support market propositions

Customer insight and regulation-driven principles underpin the core customer journeys described in four sections:

- **Authentication Methods**: The primary forms of Authentication, in generic form, that may be used through a variety of services and interactions.
- **Account Information Services (AIS)**: Service propositions that are enabled or initiated by customers (PSUs) consenting to share their payment account data with Account Information Service Providers.
- **Payment Initiation Services (PIS)**: Service propositions enabled by customers (PSUs) consenting to Payment Initiation Service Providers (PISPs) initiating payments from their payment accounts.
- **Card Based Payment Instrument Issuers (CBPIIs)**: Service propositions enabled by customers (PSUs) giving their consent to a CBPII to submit Confirmation of Funds (CoF) requests to an ASPSP.

ASPSPs should be familiar with their own role and that of other participants across all these proposition types. TPPs (AISPs, PISPs and CBPIIs) will naturally focus on the proposition types that are relevant to their business model, but they should still be aware of the roles of all participants in order to ensure they understand the lines of demarcation and differences between each type.

## The customer journey is described for each of the core use cases

Each unique journey has been broken out and described over a number of pages. They can be then be referenced in a number of ways according to individual priority e.g. whether the reader is, for example, a Regulatory Expert, Product Owner, Technical Lead or CX Designer. The page types are:

- **Journey description**: A high-level description of the specific account information, payment initiation or confirmation of funds customer journey.
- **A journey map**: This is a macro view of the customer journey, broken down by optimal steps and customer interaction points e.g. from payment initiation through authentication to completion.
- **A ‘wireframe’ journey**: This is represented by annotated ‘screens’ to identify key messages, actions, interactions and information hierarchy, as well as process dependencies.
- **Journey annotations**: This is the annotation detail referenced in the wireframes. These consist of both CEG Checklist items informing or requiring specific messaging or interactions etc. or CX considerations, where research has raised specific customer priorities or concerns that should be addressed through the eventual solution.

## The Customer Experience Checklist

The CEG Checklist takes the form of key questions that have been designated as either “required” or “recommended”. The CEG Checklist sets out which specific requirements are relevant to the Open Banking Standard Implementation Requirements, PSD2, the RTS and the CMA Order. Where relevant, it provides a regulatory reference (as per the CMA Order, PSD2/PSRs and the RTS on SCA and CSC). These are marked as either mandatory, optional or conditional in line with the definitions used across the Open Banking Standards. For TPPs, certifying against the CEG Checklist is considered as a signal of best practice to the marketplace. OBIE will consider the CEG Checklist for quality assurance and compliance purposes alongside other sources of information.

## The Customer Experience Guidelines form part of the Open Banking Standards

The Customer Experience Guidelines form part of the Open Banking Standards. The Customer Experience Guidelines (and associated Checklist form part of the Standard, and set out the customer experience required to deliver a successful Open Banking ecosystem, alongside technical, performance, non-functional requirements and dispute resolution practices. The CEG Checklist has been developed for ASPSPs and TPPs to assess compliance with this aspect of the OBIE Standard Implementation Requirements. The CEG and CEG Checklist are consistent with:

- The Revised Payment Services Directive (PSD2) (Transposed in the UK by the Payment Services Regulations 2017 (PSRs))
- The Regulatory Technical Standards on Strong Customer Authentication and Common and Secure Communication (RTS))
- The UK CMA Retail Banking Market Investigation Order which applies to the nine largest UK retail banks only (known as the CMA9)).

In developing its Standards, OBIE has undertaken extensive engagement with different market participants, and analysis to ensure that its standards have been designed in line with relevant regulatory and market requirements. On this basis, where an ASPSP seeking an exemption notifies the relevant National Competent Authority (NCA) (e.g. the FCA in the UK) that its dedicated interface follows the OBIE Standards, we expect this will provide a level of assurance that the ASPSP meets the requirement of RTS Article 30(5). Conversely, when an ASPSP has deviated from the Standards, we expect that the NCA may require additional information to enable it to consider more closely whether the ASPSP’s implementation is compliant with the relevant regulatory requirements. This may include the NCA requesting additional details on how and why there has been a deviation.

For this purpose, we would expect an ASPSP to complete and submit the CEG Checklist, providing supporting evidence as appropriate, to OBIE. This can then be provided to the NCA in support of its application for an exemption.
